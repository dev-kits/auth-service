import Token from './token'

import SuperuserVerifier from './superuser'
import MongodbUserVerifier from './mongodb'

export default {
  init: (conf) =>{
    const token = new Token(conf.secret)
    const mongo = new MongodbUserVerifier(token)
    const superuser = new SuperuserVerifier(conf.superuser, conf.superpwd, token)

    token.check = (account, password) =>{
      let ret = mongo.verify(account, password)
        .catch((err) => ret = superuser.verify(account, password))
        .then((user) => Promise.resolve(user))

      return ret
    }

    token.verify = (data) => {
      return token.decode(data)
    }

    return token
  }
}
