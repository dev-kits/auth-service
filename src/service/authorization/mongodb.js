import BaseVerifier from './base'
import MongodbUser from '../../models/user'

class MongodbUserVerifier extends BaseVerifier {
  constructor(token){
    super(null, null, token)
    this.user = null
  }

  async verify(account, pass){
    //console.log("=======> check by mongodb user: %s", account)
    try {
      // load user from db
      const user = await MongodbUser.findOne({account}).exec()
      if (user === null) {
        return Promise.reject('Account not found!')
      }

      // check password
      this.user = user;
      this.account = user.account
      this.password = user.password
      return this.check(account, pass) ? Promise.resolve(this) : Promise.reject('Password invalid!')

    } catch (err) {
      // check db user failed
      Promise.reject(err)
    }
  }

  user_info(){
    return this.user === null ? {} : this.user.toJSON()
  }
}

export default MongodbUserVerifier
