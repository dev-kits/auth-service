import BaseVerifier from './base'

class SuperuserVerifier extends BaseVerifier{
  constructor(account, password, token) {
    super(account, password, token)
  }

  verify(account, pass){
    //console.log("=======> check by Superuser: %s", account)
    return this.check(account, pass) ? Promise.resolve(this) : Promise.reject('Account or Password invalid!')
  }

  user_info(){
    return {
      id: 'super-user-6c612c83e7fb36e', account: this.account, name: 'Superuser'
    }
  }
}

export default SuperuserVerifier
