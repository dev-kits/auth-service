import {isString} from 'lodash'
import jwt from 'jsonwebtoken'

class Token {
  constructor(secret){
    this.secret = secret
  }

  encode(data){
    const now = Date.now() / 1000
    const iat = Math.floor(now)
    const exp = Math.floor(now) + (60 * 60) //1 hour

    const payload = Object.assign({iat, exp}, data)
    payload.token = jwt.sign(payload, this.secret)

    return payload
  }

  decode(token){
    try{
      return isString(token) ? jwt.verify(token, this.secret) : false
    }catch(err) {
      console.error("decode token error:", err)
      return false
    }
  }
}

export default Token