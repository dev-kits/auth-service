import encoder from '../../utils/encoder'

class BaseVerifier {
  constructor(account, password, token) {
    this.token = token
    this.account = account
    this.password = password
  }

  user_info(){
    return { account: this.account }
  }

  check(account, pass){
    return account === this.account && encoder.md5(pass) === this.password
  }

  info(){
    return this.token.encode(this.user_info())
  }
}

export default BaseVerifier
