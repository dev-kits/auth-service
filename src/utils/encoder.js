import crypto from 'crypto'

export default {
  md5: (text)=> {
    const md5 = crypto.createHash('md5')
    md5.update(text)
    return md5.digest('hex')
  }
}
