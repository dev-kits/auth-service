import _ from 'lodash'
import mongoose from 'mongoose'

mongoose.Promise = global.Promise

export default {
  init: (conf)=> {
    return mongoose.connect(conf.db_url, { promiseLibrary: global.Promise })
  },

  define: (param)=> {
    const opts = {
      timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
    }

    const Schema = mongoose.Schema
    const ret = new Schema(param, opts)
    ret.set('toJSON', { virtuals: true, getters: true, versionKey: false})
    return ret
  },

  model: (name, schema)=> {
    return mongoose.model(name, schema)
  },

  helper: (safe_fields)=> {
    return {
      cleanse: (props)=> {
        return _.reduce(props, (ret, val, key)=> {
          if(_.includes(safe_fields, key)){
            ret[key] = val
          }
          return ret
        }, {})
      },
      mass: (props, destination)=> {
        return _.reduce(props, (ret, val, key)=> {
          ret[key] = val
          return ret
        }, destination)
      },
    }
  }
}
