import jwt from 'express-jwt'
import parser from 'body-parser'
import {isString, includes, some} from 'lodash'
import Authorization from './service/authorization'

const USER_INFO_COOKIE_NAME = '_ui'
const DEFAULT_REDIRECT = '/welcome.html'

const load_user_info = (req) =>{
  const cookie = req.cookies[USER_INFO_COOKIE_NAME]
  try {
    return isString(cookie) ? JSON.parse(cookie) : null
  } catch (err) {
    console.error("Parse user info error:", err)
    return null
  }
}

const store_user_info = (res, user) =>{
  res.cookie(USER_INFO_COOKIE_NAME, JSON.stringify(user))
}

const clean_user_info = (res) =>{
  res.clearCookie(USER_INFO_COOKIE_NAME)
}

//application/json
const json_parser = parser.json()
//application/x-www-form-urlencoded
const form_parser = parser.urlencoded({extended: true})
const parsers = [form_parser, json_parser]

const getToken = (req) =>{
  const header = req.headers['Authorization']
  if (header && header.split(' ')[0] === 'Bearer') {
    return header.split(' ')[1]
  } else if (req.query && req.query['token']) {
    return req.query['token']
  }else {
    const user = load_user_info(req) || {}
    return user.token
  }
}

export default {
  init: (app, conf, excludes = []) =>{
    const custom = (req) => some(excludes, (v) => includes(req.path, v))
    const protector = jwt({getToken, secret: conf.secret}).unless({custom})
    app.use(protector)

    const service = Authorization.init(conf)

    // login.GET
    app.get('/public/login.:format?', (req, res) =>{
      const {account = '', redirect = DEFAULT_REDIRECT} = req.query
      const info = load_user_info(req) || {}
      const user = service.verify(info.token) ? info : false

      if (!user) {
        clean_user_info(res)
      }

      res.format({
        json: () =>{
          if(user){
            res.json(user)
          }else{
            res.json({message: 'please login', location: '/public/login.html'})
          }
          res.end()
        },
        html: () =>{
          if(user){
            res.status(302).location(redirect).end()
          }else{
            res.render('login', {account, redirect})
          }
        }
      })
    })

    app.get('/public/logout.:format?', parsers, (req, res) =>{
      clean_user_info(res)
      res.redirect('/public/login.html')
    })

    //login.POST
    app.post('/public/login.:format?', parsers, async (req, res) =>{
      const {account, password, redirect = DEFAULT_REDIRECT} = req.body

      let user, error
      try {
        const ret = await service.check(account, password)
        user = ret.info()
      } catch (err) {
        console.log("check account/password failure:", err)
        error = err
      }

      res.format({
        json: () =>{
          console.log('call in json')
          if (user) {
            res.json(user)
          } else {
            res.status(401).json(error)
          }
          res.end()
        },
        html: () =>{
          if (user) {
            store_user_info(res, user)
            res.status(302).location(redirect).end()
          } else {
            const message = 'account or password invalid.'
            res.render('login', {account, redirect, error, message})
          }
        }
      })
    })
  }
}
