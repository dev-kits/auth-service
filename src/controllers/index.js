import express from 'express'

export default {
  mount: (path, app) =>{
    const router = express.Router()

    // welcome
    router.get('/welcome.html', (req, res) =>{
      const {user} = req
      res.render('welcome', {user})
    })

    app.use(path, router)
  }
}