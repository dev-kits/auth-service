import path from 'path'
import morgan from 'morgan'
import express from 'express'
import favicon from 'serve-favicon'
import compression from 'compression'
import cookieParser from 'cookie-parser'

import auth from './auth'
import apis from './apis'
import conf from './config'
import controllers from './controllers'
import mongoose from './utils/mongoose'
import webserver from './utils/webserver'

const app = express()
const db = mongoose.init(conf)
const server = webserver.run(app)

app.locals.pretty = true
app.set('view engine', 'pug')
app.set('views', path.join(__dirname, 'views'))

app.use(compression())
app.use(cookieParser())
app.use(morgan('combined'))
app.use(express.static(path.join(__dirname, '../www')))
app.use(favicon(path.join(__dirname, '../www', 'public', 'favicon.ico')))

auth.init(app, conf, ['/public/'])
apis.mount('/apis', app, conf)
controllers.mount('/', app, conf)

process.on('beforeExit', ()=> {
  //db.disconnect();
});

process.on('SIGINT', ()=> {
  server.close(()=> { process.exit() })
})
