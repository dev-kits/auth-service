import express from 'express'

import user from './user'
import hello from './hello'

export default {
  mount: (path, app, conf) =>{
    const router = express.Router()

    user.mount(router, conf)
    hello.mount(router, conf)

    app.use(path, router)
  }
}