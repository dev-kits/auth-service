import parser from 'body-parser'
import User from '../models/user'

//application/json
const json_parser = parser.json()
//application/x-www-form-urlencoded
const form_parser = parser.urlencoded({extended: true})
const parsers = [form_parser, json_parser]

export default {
  mount: (router) =>{
    //users.GET
    router.get('/users', (req, res) =>{
      User.find()
        .then((docs) => res.json(docs).end())
        .catch((err) => res.status(500).json(err).end());
    })

    //users.POST
    router.post('/users', json_parser, (req, res) =>{
      console.log(req.body)
      User.safe_create(req.body)
        .then(user => res.json(user).end())
        .catch(err => res.status(400).json(err).end())
    })

    //users.PUT
    router.put('/users/:id', json_parser, (req, res) =>{
      const id = req.params.id;
      User.update_by(id, req.body)
        .then(user => res.json(user).end())
        .catch(err => res.status(404).json(err).end())
    })

    //users.DELETE
    router.delete('/users/:id', (req, res) =>{
      User.findByIdAndRemove(req.params.id).exec()
        .then(user => res.json(user.id).end())
        .catch(err => res.status(500).json(err).end())
    })
  }
}
