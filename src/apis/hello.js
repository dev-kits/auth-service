export default {
  mount: (router) =>{
    // hello
    router.get('/hello', (req, res) =>{
      const {user} = req
      res.json({message: `Hello ${user.name}`})
    })
  }
}