import _ from 'lodash'
import encoder from '../utils/encoder'
import mongoose from '../utils/mongoose'

const fields = {
  name: {type: String, required: true}, //姓名
  email: {type: String, required: false}, //邮箱
  type: {type: String, required: false}, //类型
  phone: {type: String, required: true}, // 电话
  password: {type: String, required: true}, //密码
  account: {type: String, required: true, unique: true}, //帐号
}

const safe_fields = Object.keys(fields)
const schema = mongoose.define(fields)
const {cleanse, mass} = mongoose.helper(safe_fields)

schema.statics.safe_create = (props, auto_save = true) =>{
  const user = new User(cleanse(props))
  user.password = encoder.md5(user.password)

  return auto_save ? user.save() : Promise.resolve(user)
};

schema.statics.update_by = (id, props, auto_save = true) =>{
  return new Promise((resolve, reject) =>{
    User.findById(id).exec().then((user) =>{
      if (_.isNil(user)) {
        return reject(new Error(`Not found ${id}`))
      }

      // encode the password
      if (_.has(props, 'password')) {
        props.password = encoder.md5(props.password)
      }

      //TODO: role 不能随意改，检查是否是超级用户

      // mass-assignment values
      user = mass(cleanse(props), user);
      if (!auto_save) {
        return resolve(user)
      }

      // save to db...
      user.save()
        .then(() =>{ resolve(user) })
        .catch((err) =>{ reject(err) })

    }).catch((err) =>{ reject(err) })
  })
}

const User = mongoose.model('User', schema)
export default User
