const default_values = {
  port: '3000',
  superuser: "admin",
  db_url: 'mongodb://localhost/livedb',
  secret: "change_this_text_in_production",
  superpwd: 'a3175a452c7a8fea80c62a198a40f6c9' //'admin@admin'
};

module.exports = {
  db_url: process.env.DB_URL || default_values.db_url,
  secret: process.env.SECRET_SALT || default_values.secret,
  superpwd: process.env.SUPER_USER_PWD || default_values.superpwd,
  superuser: process.env.SUPER_USER_NAME || default_values.superuser
};
