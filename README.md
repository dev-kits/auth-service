# auth
A simple user authentication service base on JWT.

### Tech Stack
* [nodejs](https://nodejs.org)
* [mongodb](https://www.mongodb.com)
* [docker](https://www.docker.com)
* [docker-compose](https://github.com/docker/compose/releases)

###
