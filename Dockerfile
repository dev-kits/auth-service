FROM node:7-alpine
MAINTAINER "Live platform Team" <hw-live@thoughtworks.com>

# Prepare work directory and copy all files
RUN mkdir /app
WORKDIR /app
COPY . .

# install dependencies
RUN npm install --production

#
CMD ["npm", "run", "start"]
